class Juggler

  attr_accessor :drum

  def perform
    #drum.juggle
    puts 'Juggling'
  end
end

class RdiContext

  attr_reader :config

  def initialize(config)
    @config = config
    @beans = {}

    load_config(config)
  end

  def get(bean_id)
    bean = @beans[bean_id]
    bean.evaluate
  end

  private
  def load_config(config)
    instance_eval File.read(config)
    #load config
  end

  def bean(bean_id, klass)
    puts "bean"
    @beans[bean_id] = RdiBean.new(bean_id, klass)
  end
end

class RdiBean
  def initialize(bean_id, klass)
    @bean_id = bean_id
    @klass = klass
  end

  def evaluate
    @bean ||= create
  end

  private
  def create
    @klass.new
  end
end

rdi_context = RdiContext.new('rdi.rb')
duke = rdi_context.get(:duke)

duke.perform
