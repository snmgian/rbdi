module Rdi
  class Ref

    attr_reader :bean_id

    def initialize(bean_id)
      @bean_id = bean_id
    end

    def eql?(other)
      if other.respond_to?(:bean_id)
        bean_id.eql?(other.bean_id)
      else
        false
      end
    end

    def hash
      bean_id.hash
    end

  end
end
