module Rdi
  module Utils

    # Extracts values from a given array
    #
    #
    # Suppose there is a method named 'm'
    #
    #   def m(id, name, options); end
    #
    # Which is expected to be called as:
    #
    #   m(1, 'foo', :x => true, :y => true)
    #
    # and also:
    #
    #   m('foo')
    #
    # In the second case, 'foo' is expected to be pased as the 'name' parameter, 
    #   and id parameter is expected to be 0 (zero)
    #
    # It would be good to define m as:
    #
    #  def m(id = nil, name, options = {})
    #
    # So, we start to implement m as:
    #
    #   def m(*args)
    #
    #     id = 0
    #     name = nil
    #     options = {}
    #
    #     if args.empty?
    #       raise ArgumentError, 'wrong number of arguments (0 for 1..3)'
    #     end
    #
    #     if args.length == 1
    #       name = args[0]
    #     elsif args.length == 2
    #       id = args[0]
    #       name = args[1]
    #     elsif args.length == 3
    #       id = args[0]
    #       name = args[1]
    #       options = args[3]
    #     else
    #       raise ArgumentError, "wrong number of arguments (#{args.length} for 1..3)"
    #     end
    #
    #     # ...
    #   end
    #
    # By using extract_args, the above code could be refactored as follows:
    #
    #   def m(*args)
    #     id, name, options = extract_args(args, 3, {:0 => nil, :2 => {}})
    #
    #     // ...
    #   end
    #
    # @param [Array] args Arguments passed
    # @param [Integer] args_number Maximum number of expected arguments
    # @param [Hash<Integer, Object>] Default argument values.
    #   keys => index of the argument
    #   values => argument value
    #
    # @return [Array] Extracted arguments
    #
    # @raise [ArgumentError] if less params than expected are passed
    def self.extract_args(args, args_number, defaults = {})
      min_args_number = args_number - defaults.length
      
      if args.length < min_args_number
        raise ArgumentError, "wrong number of arguments (#{args.length} for #{min_args_number}..#{args_number}"
      elsif args.length > args_number
        raise ArgumentError, "wrong number of arguments (#{args.length} for #{min_args_number}..#{args_number}"
      end

      # extracted args are put here
      values = Array.new(args_number)

      default_indexes = defaults.keys.sort
      non_default_indexes = (0...args_number).to_a - default_indexes

      defaults.each do |index, default|
        values[index] = default
      end

      # number of args that will override default args
      extra_args = args.length - (args_number - defaults.length)

      # TODO: make this more legible
      args.each_with_index do |arg, index|
        if extra_args > 0
          possible_default_index = default_indexes.first || (args_number + 1)
          possible_non_default_index = non_default_indexes.first || (args_number + 1)
          if possible_default_index < possible_non_default_index
            values[possible_default_index] = arg
            default_indexes.delete_at(0)
            extra_args -= 1
          else
            values[possible_non_default_index] = arg
            non_default_indexes.delete_at(0)
          end
        else
          possible_non_default_index = non_default_indexes.delete_at(0)
          values[possible_non_default_index] = arg
        end
      end

      values
    end

  end
end
