require 'set'

require_relative 'ref'

module Rdi
  class Bean

    def initialize(ctx, bean_id, klass, options)
      @ctx = ctx
      @bean_id = bean_id
      @klass = klass
      @options = options
      @options[:scope] ||= :singleton

      @construct_args = []
      @init_method = nil
      @destroy_method = nil
      @properties = {}
    end

    def arg(value)
      @construct_args << value
    end

    def bean(*args, &block)
      @ctx.bean(*args, &block)
    end

    def destroy_method(method_name)
      @destroy_method = method_name
    end

    def dispose
      if @bean && @destroy_method
        @bean.send @destroy_method
      end

      @bean = nil
    end

    def init_method(method_name)
      @init_method = method_name
    end

    def property(name, value)
      @properties[name] = value
    end

    def ref(bean_id)
      Rdi::Ref.new(bean_id)
    end

    def evaluate
      if @options[:scope] == :prototype
        create
      else
        @bean ||= create
      end
    end

    private
    def create
      args = @construct_args.map do |arg|
        if arg.kind_of?(Rdi::Ref)
          @ctx.get(arg.bean_id)
        else
          arg
        end
      end
      bean = if @options.include?(:factory_method)
        @klass.send(@options[:factory_method])
      else
        @klass.new(*args)
      end

      @properties.each do |name, value|
        if value.kind_of?(Rdi::Ref)
          value = @ctx.get(value.bean_id)
        elsif value.kind_of?(Rdi::Bean)
          value = value.evaluate
        elsif value.kind_of?(Array) || value.kind_of?(Set)
          value = value.map do |v|
            if v.kind_of?(Rdi::Ref)
              @ctx.get(v.bean_id)
            elsif v.kind_of?(Rdi::Bean)
              v.evaluate
            else
              v
            end
          end
        elsif value.kind_of?(Hash)
          value.each do |k, v|
            if v.kind_of?(Rdi::Ref)
              value[k] = @ctx.get(v.bean_id)
            elsif v.kind_of?(Rdi::Bean)
              value[k] = v.evaluate
            end
          end
        end

        bean.send "#{name}=", value 
      end

      if @init_method
        bean.send @init_method
      end

      bean
    end

  end
end
