require_relative 'bean'
require_relative 'ref'

# 'bean' can refer to:
#  x ruby object
#  - smth that can be evaluated and disposed within a context
#  - a definition of a managed ruby object
module Rdi
  class Context

    attr_reader :config

    def initialize(config)
      @config = config
      @beans = {}

      load_config(config)
    end

    def get(bean_id)
      bean = @beans[bean_id]
      bean.evaluate
    end

    def close
      @beans.each do |bean_id, bean|
        bean.dispose
      end
    end

    # def bean(bean_id, klass, options = {}, &block)
    def bean(*args, &block)
      bean_id, klass, options = extract_args(args, 3, { 0 => nil, 2 => {} })
      bean = Rdi::Bean.new(self, bean_id, klass, options)
      if block_given?
        bean.instance_eval &block
      end
      if bean_id
        @beans[bean_id] = bean
      else
        bean
      end
    end

    private
    def load_config(config)
      instance_eval File.read(config)
    end

  end
end
