module Rdi
  class Bean

    attr_reader :constructor_args
    attr_reader :id
    attr_reader :properties
    attr_reader :klass

    attr_accessor :destroy_method
    attr_accessor :init_method
    attr_accessor :options

    def initialize(id, klass, options = {})
      @id = id
      @klass = klass
      @options = options
      @options[:scope] ||= :singleton

      @construct_args = []
      @init_method = nil
      @destroy_method = nil
      @properties = {}
    end

  end
end
