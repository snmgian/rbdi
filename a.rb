class A < BasicObject

  def !
    ::Kernel.puts "!!"
    super
  end
end

class A
  remove_instance_method :!=
end

sa = class << A
  self
end
(x = A.new) && 1

class Y
  def y
    "y"
  end
  remove_method :y
end

