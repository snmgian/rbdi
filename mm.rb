class A
  def method_missing(name, *args, &block)
    caller
  end
end

class B
  def b
    A.new.o
  end
end

b = B.new
p b.b

p '--'

a = A.new
p a.method_missing(nil)
