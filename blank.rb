class FromBasic < BasicObject
end

p FromBasic.instance_methods

class Blank

  instance_methods.each do |im|
    unless im =~ /^__send__|^method_missing$|^object_id$/
      undef_method(im)
    end
  end

  def method_missing(m, *args, &block)
    ::Kernel::p m
  end
end

p Blank.instance_methods
b = Blank.new
p b
