class Link
  def initialize(m, args, block)
    @m = m
    @args = args
    @block = block
  end

  def resolve(context)
    context.send(@m, *@args, &@block)
  end
end

class Chain < BasicObject
  def initialize(initial_context)
    ::Kernel::puts "i"
    @links = []
    @initial_context = initial_context
    #self
  end

  def method_missing(m, *args, &block)
    ::Kernel.p m
    if !m.nil?
      @links << ::Link.new(m, args, block)
      self
    else
      current_context = @initial_context
      ::Kernel.p current_context, '--'
      @links.each do |link|
        current_context = link.resolve(current_context)
      end
      current_context
    end
  end
end

class ChainResolver
  def initialize(chain)
    @chain = chain
  end

  def resolve
    @chain.method_missing(nil)
  end
end

(chain = Chain.new(0)) && 0
(chain = chain.+(1).+(3).-(2)) && 0
chain.method_missing(nil)
# p chain.class


resolver = ChainResolver.new(chain)
p resolver.resolve
