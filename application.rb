class Auditorium
  def turn_on_lights
    puts "Turn on lights"
  end

  def turn_off_lights
    puts "Turn off lights"
  end
end

class Instrumentalist
  attr_accessor :instrument
  attr_accessor :song

  def perform
    print "Playing #{song}: "
    instrument.play
  end

  def scream_song
    song
  end
end

class Juggler
  def initialize(bean_bags = 3)
    @bean_bags = bean_bags
  end

  def perform
    puts "Juggling #{@bean_bags} beanbags"
  end
end

class OneManBand
  attr_accessor :instruments

  def perform
    instruments.each do |instrument|
      instrument.play
    end
  end
end

class OneManBandVerbose
  attr_accessor :instruments

  def perform
    instruments.each do |key, instrument|
      print "#{key}: "
      instrument.play
    end
  end
end


class Cymbal
  def play
    puts 'CRASH CRASH CRASH'
  end
end

class Guitar
  def play
    puts 'STRUM STRUM STRUM'
  end
end

class Harmonica
  def play
    puts 'HUM HUM HUM'
  end
end

class Piano
  def play
    puts 'PLINK PLINK PLINK'
  end
end

class PoeticJuggler < Juggler
  def initialize(poem, bean_bags = nil)
    if bean_bags.nil?
      super()
    else
      super(bean_bags)
    end

    @poem = poem
  end

  def perform
    super
    puts 'While reciting ...'
    @poem.recite
  end
end

class Saxophone
  def play
    puts "TOOT TOOT TOOT"
  end
end

class Sonet29
  CONTENT = <<-eos
    When, in disagree with,
    I all alone
  eos

  def recite
    puts CONTENT
  end
end

class Stage

  def self.build
    @stage ||= Stage.new
  end
end

class Ticket

  @@ticket_counter = 0

  attr_reader :number

  def initialize
    @@ticket_counter += 1
    @number = @@ticket_counter
  end
end
