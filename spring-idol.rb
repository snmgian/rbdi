require 'application'
require 'set'

bean(:auditorium, Auditorium) {
  init_method :turn_on_lights
  destroy_method :turn_off_lights
}

#bean(:carl, Instrumentalist) {
  #property :song, ref(:kenny).song
#}

bean(:cymbal, Cymbal)

bean(:duke, Juggler) {
  arg 15
}

bean(:guitar, Guitar)

bean(:hank, OneManBand) {
  property :instruments, [
    ref(:guitar),
    ref(:cymbal),
    ref(:harmonica),
  ]
}

bean(:hank_unique, OneManBand) {
  property :instruments, Set.new([
    ref(:guitar),
    ref(:cymbal),
    ref(:guitar),
  ])
}

bean(:hank_verbose, OneManBandVerbose) {
  property :instruments, {
    guitar: ref(:guitar),
    cymbal: ref(:cymbal),
    harmonica: ref(:harmonica),
  }
}

bean(:harmonica, Harmonica)

bean(:kenny, Instrumentalist) {
  property :song, 'Jingle Bells'
  property :instrument, ref(:piano)
}

bean(:kenny_egoist, Instrumentalist) {
  property :song, 'Jingle Bells'
  property :instrument, bean(Piano)
}

bean(:kenny_with_nil, Instrumentalist) {
  property :song, 'JingleBells'
  property :instrument, nil
}

bean(:piano, Piano)

bean(:poetic_duke, PoeticJuggler) {
  arg ref(:sonet_29)
  arg 15
}

bean(:saxophone, Saxophone)

bean(:sonet_29, Sonet29)

bean(:the_stage, Stage, :factory_method => :build)

bean(:ticket, Ticket, :scope => :prototype)
