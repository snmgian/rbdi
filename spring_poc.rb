$: << File.dirname(__FILE__)
$: << File.join(File.dirname(__FILE__), 'lib')

require 'rdi'

context = Rdi::Context.new('spring-idol.rb')

auditorium = context.get(:auditorium)

performer = context.get(:duke)
performer.perform

poetic_duke = context.get(:poetic_duke)
poetic_duke.perform

the_stage = context.get(:the_stage)
p the_stage
p the_stage

ticket_1 = context.get(:ticket)
ticket_2 = context.get(:ticket)
p ticket_1.number
p ticket_2.number

piano = context.get(:piano)

kenny = context.get(:kenny)
kenny.perform

kenny_egoist = context.get(:kenny_egoist)

p(kenny.instrument.object_id != kenny_egoist.instrument.object_id)
p(piano.object_id == kenny.instrument.object_id)

kenny_with_nil = context.get(:kenny_with_nil)
p kenny_with_nil.instrument

hank = context.get(:hank)
hank.perform

hank_unique = context.get(:hank_unique)
hank_unique.perform

hank_verbose = context.get(:hank_verbose)
hank_verbose.perform


context.close
