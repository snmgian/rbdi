require 'spec_helper'

describe Rdi::Bean do

  describe '.new' do
    it 'sets id' do
      bean = Rdi::Bean.new(:id, Object)

      expect(bean.id).to eq(:id)
    end

    it 'sets klass' do
      bean = Rdi::Bean.new(:id, Object)

      expect(bean.klass).to eq(Object)
    end

    context 'with no scope' do
      it 'sets scope option to :singleton' do
        bean = Rdi::Bean.new(:id, Object)

        expect(bean.options[:scope]).to eq(:singleton)
      end
    end

    context 'with scope' do
      it 'sets scope option to the given scope' do
        bean = Rdi::Bean.new(:id, Object, :scope => :prototype)

        expect(bean.options[:scope]).to eq(:prototype)
      end
    end
  end
end
