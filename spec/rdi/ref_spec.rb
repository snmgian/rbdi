require 'spec_helper'

describe Rdi::Ref do

  describe '#eql?' do
    it 'is true for the same ids' do
      ref_a = Rdi::Ref.new(:ref)
      ref_b = Rdi::Ref.new(:ref)

      expect(ref_a.eql?(ref_b)).to be_true
    end

    it 'is false for different ids' do
      ref_a = Rdi::Ref.new(:ref_a)
      ref_b = Rdi::Ref.new(:ref_b)

      expect(ref_a.eql?(ref_b)).to be_false
    end

    it 'is false when compared with a non bean' do
      ref_a = Rdi::Ref.new(:ref_a)
      ref_b = double(:ref_b)

      expect(ref_a.eql?(ref_b)).to be_false
    end
  end

  describe '#hash' do
    it 'is the same for the same ids' do
      hash_a = Rdi::Ref.new(:ref).hash
      hash_b = Rdi::Ref.new(:ref).hash

      expect(hash_a).to eq(hash_b)
    end

    it 'is different for different ids' do
      hash_a = Rdi::Ref.new(:ref_a).hash
      hash_b = Rdi::Ref.new(:ref_b).hash

      expect(hash_a).to_not eq(hash_b)
    end
  end
end
