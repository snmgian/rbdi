require 'spec_helper'

describe Rdi::Utils do
  describe '.extract_args' do

    context 'a = nil, b, c = {}' do
      let(:args_number) { 3 }

      let(:args_spec) do
        { 0 => :default_0, 2 => :default_2 }
      end

      context 'with all params' do
        it 'returns the given values' do
          a, b, c = Rdi::Utils.extract_args([:a, :b, :c], args_number, args_spec)
          expect(a).to eq(:a)
          expect(b).to eq(:b)
          expect(c).to eq(:c)
        end
      end

      context 'with 2nd param' do
        it 'returns the default params + the 2nd param' do
          a, b, c = Rdi::Utils.extract_args([:b], args_number, args_spec)
          expect(a).to eq(:default_0)
          expect(b).to eq(:b)
          expect(c).to eq(:default_2)
        end
      end

      context 'with 1st and 2nd param' do
        it 'returns the params + the default 3rd param' do
          a, b, c = Rdi::Utils.extract_args([:a, :b], args_number, args_spec)
          expect(a).to eq(:a)
          expect(b).to eq(:b)
          expect(c).to eq(:default_2)
        end
      end

      it 'discards parameters when more parameters than expected are passed' do
        expect {
          Rdi::Utils.extract_args([:a, :b, :c, :d], args_number, args_spec)
        }.to raise_error(ArgumentError)
      end

      it 'raises ArgumentError when there is no enough parameters' do
        expect {
          Rdi::Utils.extract_args([], args_number, args_spec)
        }.to raise_error(ArgumentError)
      end
    end

    context 'a = default, b = default, c = default' do
      let(:args_number) { 3 }

      let(:args_spec) do
        { 0 => :default_0, 1 => :default_1, 2 => :default_2 }
      end

      context 'with all params' do
        it 'returns the given values' do
          a, b, c = Rdi::Utils.extract_args([:a, :b, :c], args_number, args_spec)
          expect(a).to eq(:a)
          expect(b).to eq(:b)
          expect(c).to eq(:c)
        end
      end

      context 'with 1st param' do
        it 'returns the default params + the 1st param' do
          a, b, c = Rdi::Utils.extract_args([:a], args_number, args_spec)
          expect(a).to eq(:a)
          expect(b).to eq(:default_1)
          expect(c).to eq(:default_2)
        end
      end

      context 'with 1st and 2nd param' do
        it 'returns params 1, 2, and default 3rd param' do
          a, b, c = Rdi::Utils.extract_args([:a, :b], args_number, args_spec)
          expect(a).to eq(:a)
          expect(b).to eq(:b)
          expect(c).to eq(:default_2)
        end
      end

      context 'with no params' do
        it 'returns default params' do
          a, b, c = Rdi::Utils.extract_args([], args_number, args_spec)
          expect(a).to eq(:default_0)
          expect(b).to eq(:default_1)
          expect(c).to eq(:default_2)
        end
      end

      it 'when more parameters than expected are passed' do
        expect {
          Rdi::Utils.extract_args([:a, :b, :c, :d], args_number, args_spec)
        }.to raise_error(ArgumentError)
      end
    end

    context 'with no default args' do
      let(:args_number) { 3 }

      let(:args_spec) do
        Hash.new
      end

      context 'with all params' do
        it 'returns the given values' do
          a, b, c = Rdi::Utils.extract_args([:a, :b, :c], args_number, args_spec)
          expect(a).to eq(:a)
          expect(b).to eq(:b)
          expect(c).to eq(:c)
        end
      end

      it 'raises ArgumentError when more parameters than expected are passed' do
        expect {
          Rdi::Utils.extract_args([:a, :b, :c, :d], args_number, args_spec)
        }.to raise_error(ArgumentError)
      end

      it 'raises ArgumentError when no enough parameters' do
        expect {
          Rdi::Utils.extract_args([:a], args_number, args_spec)
        }.to raise_error(ArgumentError)
      end
    end

  end
end
